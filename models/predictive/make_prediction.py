#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pandas as pd
import pickle
from sklearn.preprocessing import LabelEncoder
import numpy as np

# encode sample's categorical data
def label_encoder(y, col_name, encoders_directory):
    encoder = LabelEncoder()
    encoder.classes_ = np.load(encoders_directory+'/'+col_name+'.npy', allow_pickle=True)
    y_enc = encoder.transform(list(y))
    y = int(y_enc[0])
    return y

def predict(model_directory, inputs):

    selected_cols = pd.read_csv(model_directory + r'/column_names.csv',header=None)

    to_encode = ['Διάστημα άφιξης ασθενή στα ΤΕΠ', 'Ιστορικό καπνίσματος', 
                'Ιστορικό κατανάλωσης αλκοόλ', 'Κατηγορία άφιξης',
                'Ύπαρξη πολυφαρμακίας;', 'Φύλο']

    # encode variables
    for col_name in to_encode:
        encoders_directory = model_directory + r'/encoders'
        inputs.loc[:,col_name] = label_encoder(inputs.loc[:,col_name], col_name, encoders_directory)

    #load knn imputer
    with open(model_directory + r'/KNN_imputer.pickle', 'rb') as f:   #test_list_feat_selec_2fold
        imputer = pickle.load(f)

    #imputation
    cols= inputs.columns
    inputs = imputer.transform(inputs)
    inputs = pd.DataFrame(inputs)
    inputs.columns = cols

    #select variables
    inputs = inputs[selected_cols.iloc[:,0]]

    #load predictiven model
    with open(model_directory + r'/RF.pickle', 'rb') as f:   #test_list_feat_selec_2fold
        model = pickle.load(f)

    # make prediction
    y_pred = model.predict(inputs)[0]
    if y_pred == 1: output = 'Admission'
    elif y_pred == 0: output = 'Discharge'
    return output
