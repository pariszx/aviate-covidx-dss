#!/usr/bin/env python
# -*- coding: utf-8 -*-

from make_prediction import *
import os
import pandas as pd
import pika
import json


connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()
channel.queue_declare(queue='inputs')
channel.queue_declare(queue='results')

def callback(ch, method, properties, body):
  
	print('got message')

	model_directory = os.getcwd()

	print(model_directory)

	inputs = pd.read_json(body, encoding='utf-8', typ='series')
	print(inputs)

	#series to dataframe
	inputs = pd.DataFrame(inputs).T

	output = predict(model_directory, inputs)

	print('my response is: ', output)

	# send a message back
	channel.basic_publish(exchange='', routing_key='results', body=output)

# receive message and complete simulation
channel.basic_consume(on_message_callback=callback, queue='inputs', auto_ack=True)
channel.start_consuming()
