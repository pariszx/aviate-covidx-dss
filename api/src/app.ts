import express from 'express';
import 'express-async-errors';
import { json } from 'body-parser';
import { dssRouter } from './routes/dss';
import { signinRouter } from './routes/signin';
import { signupRouter } from './routes/signup';
import { errorHandler } from './middlewares/error-handler';
import { currentUserRouter } from './routes/current-user';
import cookieSession from 'cookie-session';
import { signoutRouter } from './routes/signout';
import { fhirRouter } from './routes/fhir';
import { currentUser } from './middlewares/current-user';

const app = express();
app.set('trust proxy', true);
app.use(json());
app.use(
  cookieSession({
    signed: false,
    secure: false
  })
);
app.use(currentUser);

app.use(fhirRouter);
app.use(dssRouter);
app.use(signupRouter);
app.use(signinRouter);
app.use(signoutRouter);
app.use(currentUserRouter);

app.all('*', async (req, res) => {
  res.status(404).json({ 'error': 'Not found' });
  return;
});

app.use(errorHandler);

export { app };
