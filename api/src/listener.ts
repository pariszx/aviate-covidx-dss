import amqp from 'amqplib';

amqp.connect('amqp://localhost').then((conn: any) => {
  console.log('connected');
  conn.createChannel().then((ch: any) => {
    
    const simulations = 'simulations';
    const ok = ch.assertQueue(simulations, {durable: false}).then((_qok: any) => {
      return ch.consume(simulations, (msg: any) => {
        console.log(" [x] Received '%s'", msg.content.toString());
      }, {noAck: true});
    });

    return ok.then((_consumeOk: any) => {
      console.log(' [*] Waiting for messages. To exit press CTRL+C');
    });
  });
}).catch(err => console.error(err));