import express, { Request, Response } from 'express';
import { db } from '../database';
import amqp from 'amqplib';
var Fhir = require('fhir').Fhir;

const router = express.Router();

router.post('/api/fhir/validate', async (req: Request, res: Response) => {

  var resource = req.body;
  var fhir = new Fhir();
  // var xml = fhir.objToXml(resource);
  // var json = fhir.xmlToJson(xml);
  // var obj = fhir.xmlToObj(xml);
  var results = fhir.validate(resource, { errorOnUnexpected: true });
  results = fhir.validate(resource, {});

  res.status(200).json({ 'message': 'success', 'data': results });
});

export { router as fhirRouter };
