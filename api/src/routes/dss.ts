import express, { Request, Response } from 'express';
import amqp from 'amqplib';
import { BadRequestError } from '../errors/bad-request-error';
import { requireAuth } from '../middlewares/require-auth';

const router = express.Router();

router.post('/api/dss',
  requireAuth,
  async (req: Request, res: Response) => {

  console.log('ill try to connect to rabbitmq...');

  const conn = await amqp.connect('amqp://localhost');

  console.log('connected');

  const channel = await conn.createChannel();

  console.log('channel created');

  const inputs = 'inputs';
  const results = 'results';

  channel.assertQueue(inputs, { durable: false });
  channel.assertQueue(results, { durable: false })

  // const message = new Date().toISOString();

  channel.sendToQueue(inputs, Buffer.from(JSON.stringify(req.body)));

  console.log(`Message ${Buffer.from(JSON.stringify(req.body))} was sent`);

  channel.consume(results, (msg: any) => {
    console.log(`Message ${msg.content.toString()} was received`);
    res.json({
      'status': 'OK',
      'data': msg.content.toString()
    });
    channel.close();
  }, { noAck: true });
});

export { router as dssRouter };
